import AppHome from './components/Pages/AppHome'
import AppAbout from './components/Pages/AppAbout'
import AppGallery from './components/Pages/AppGallery'
import NotFound from './components/Pages/NotFound'

const routes = [
    {
        path: '/',
        name: 'home',
        component: AppHome,
    },
    {
        path: '/about',
        name: 'about',
        component: AppAbout,
    },
    {
        path: '/gallery',
        name: 'gallery',
        component: AppGallery,
    },
    {
        path: '/404',
        component: NotFound,
    },
    {
        path: '*',
        redirect: '/404',
    }

];

export default routes;