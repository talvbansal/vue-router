
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
import routes from './routes';

Vue.use(VueRouter);

const router = Vue.router = new VueRouter({
	routes
});

import AppIndex from './components/AppIndex';
import AppCard from './components/AppCard';

/* eslint-disable no-new */
const app = new Vue({
    el: '#app',
    router,
    render: h => h(AppIndex),
});
